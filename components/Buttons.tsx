/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React from 'react';
import {
    StyleSheet,
    Text,
    Pressable,
    View,
} from 'react-native';
import {Button} from "react-native-paper";

const ResetFunc = () => {
    console.log('RESET')
}
const SwapFunc = () => {
    console.log('SWAP')
}




function ButtonMine(text: string, func: Function, color: string): JSX.Element {
    return (
        <Pressable style={[styles.button, {backgroundColor: color}]} onPress={func}>
            <Text style={styles.buttonText}>
                {text}
            </Text>
        </Pressable>
    )
}

function Buttons(): JSX.Element {
    return (
        <View style={styles.main}>
            <Button mode={'contained'}>
                mt
            </Button>
            {/*{Button("SWAP", SwapFunc, 'red')}*/}
            {/*{Button("RESET", ResetFunc, 'gray')}*/}
        </View>

    );
}

const styles = StyleSheet.create({
    main: {
        flexDirection: 'row',
        justifyContent: 'space-around'

    },
    button: {
        width: 180,
        height: 57,
        borderRadius: 8,
        justifyContent: 'center'

    },
    buttonText: {
        fontSize: 18,
        color: "white",
        fontWeight: 'bold',
        textAlign: 'center',
    }

});

export default Buttons;