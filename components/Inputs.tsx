/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React, {useState} from 'react';
import {
    StyleSheet, Text,
    TextInput,
    View,
} from 'react-native';

const MakeConversion = () => {
    console.log('CONVERSION')
}


type InputProps = {
    label: string
}


const Input: React.FC<InputProps> = ({label}) => {
    const [focused, setFocused] = useState(false)
    const inputStyles = focused ? [styles.input, styles.inputFocused]: [styles.input]

    return (
        <View style={styles.inputContainer}>
            <Text style={styles.label}> {label} </Text>
            <TextInput onFocus={() => {
                setFocused(true)
            }} onBlur={() => {
                setFocused(false)
            }} onChangeText={MakeConversion} style={inputStyles} keyboardType='numeric'/>
        </View>

    );
}

function Inputs(): JSX.Element {
    return (
        <View style={styles.main}>
            <Input key={"input1"} label={"km"}/>
            <Input key={"input2"} label={"m"}/>

        </View>

    );
}

const styles = StyleSheet.create({

    main: {
        backgroundColor: 'transparent',
        flexDirection: 'row',
        justifyContent: 'space-around',
        width: '100%'

    },
    input: {
        fontSize: 18,
        borderBottomColor: 'white',
        borderBottomWidth: 1,
        width: '100%',
        color: 'white'

    },

    inputFocused: {
        borderBottomColor: 'orange',
    },


    inputContainer: {
        width: '45%',


    },

    label: {
        fontSize: 15,
        color: "white",
        fontStyle: 'normal',
        textAlign: 'center'
    }


});

export default Inputs;