/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React from 'react';
import {
    StyleSheet,
    Text,
    View,
} from 'react-native';


function Header(): JSX.Element {
    return (
        <View style={styles.main}>
            <Text style={styles.text}>
                Square
            </Text>

        </View>

    );
}

const styles = StyleSheet.create({
    main: {
        paddingTop: 50,
        height: 80,
        backgroundColor: 'red'
    },
    text: {
        fontSize: 18,
        color: "white",
        fontStyle: 'normal',
        fontWeight: 'bold',
        textAlign: 'center'
    }

});

export default Header;