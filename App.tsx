/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    View,
} from 'react-native';
import Header from './components/Header'
import Buttons from "./components/Buttons";
import Inputs from "./components/Inputs";

function App(): JSX.Element {
    return (
        <SafeAreaView style={styles.safeAreaView}>
            <Header/>
            <Inputs/>

            <View style={styles.mainView}>


            </View>
            <Buttons/>



        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    mainView: {
        flex: 1
    },

    safeAreaView: {flex: 1, backgroundColor: 'black'}

});

export default App;
